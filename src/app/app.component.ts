import { Component } from '@angular/core';

export interface Card {
  title: string;
  text: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  toggle = true;

  cards: Card[] = [
    {title: 'Card 1', text: 'This is card 1'},
    {title: 'Card next', text: 'Card with no number'},
    {title: 'Last card', text: 'The last card'}
  ]

  toggleCards() {
    this.toggle = !this.toggle;
  }
}
